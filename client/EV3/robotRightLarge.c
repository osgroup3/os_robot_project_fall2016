#include <unistd.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <pthread.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "robot.h"
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

//////////////////////////////////////////////////
#endif

#define SERV_ADDR "98:01:a7:9f:09:00"  /* Whatever the address of the server is */
#define TEAM_ID     3                       /* Your team ID */

// Stuff to change according to lab results
#define BALL_COLOR "RED"
#define OTHER_BALL_COLOR "BLUE"
#define OFFSET_FROM_BALL 18 // larger offset means go further when grabbing ball
#define CLAW_SPEED_MULTIPLIER .75
#define SPEED_LINEAR_MULTIPLIER .3
#define SPEED_CIRCULAR_MULTIPLIER .125
#define DEGREE_TO_COUNT(d) ((d) * 2.3 )
#define DISTANCE_TO_PULSES(d) ((int)roundf((d) * 21 )) // convert from cm to tacho pulses
#define CLAW_PULSES 75
#define SMALL_CLAW_PULSES 10
#define SMALL_CLAW_TIME 100
#define FAST_CLAW_SPEED_MULTIPLIER .8
#define SLOW_CLAW_SPEED_MULTIPLIER .1
#define BACKUP_DISTANCE -17
#define FAST_CLAW_SLEEP_TIME 2000
#define SLOW_CLAW_SLEEP_TIME 4000
#define NUMBER_OF_SENSOR_GETS 3
#define TRACK 11.7 // separation between wheels in cm


// Stuff we aren't currently using
#define CLAW_RAMP_TIME 200
#define CLAW_RUN_TIME 1000
#define MOTOR_PULSES 90
#define WHEEL_RAMP_TIME 1000

// Stuff that shouldn't change
#define R 0
#define L 1 
const char const *color[] = { "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" };
#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))
#define A 65 // none
#define B 66 // right wheel
#define C 67 // claw
#define D 68 // left wheel

#ifndef M_PI
#    define M_PI 3.14159265358979323846
#endif

#define MSG_ACK             0
#define MSG_NEXT            1
#define MSG_START           2
#define MSG_STOP            3
#define MSG_CUSTOM          4
#define MSG_KICK            5
#define MSG_POSITION        6
#define MSG_BALL            7

#define BOTTOM_LEFT			0
#define BOTTOM_RIGHT		1
#define TOP_LEFT			2
#define TOP_RIGHT			3

#define INET_PORT   8888

const double ENCODER_SCALE_FACTOR = (M_PI * 5.6) / 360.0; // convert from tacho pulses to cm

short msgToAck;

int max_speed_motor;
int max_speed_wheelR;
int max_speed_wheelL;

FLAGS_T state_wheel_L;
FLAGS_T state_wheel_R;
FLAGS_T state_motor;

uint8_t sn_motor;
uint8_t sn_wheels[2];
uint8_t sn_touch;
uint8_t sn_color;
uint8_t sn_compass;
uint8_t sn_sonar;
uint8_t sn_gyro;

int nextMessageCount = 0;
int turnAngle;

char strBuf[ 256 ];
int val;
int i;
float value;
uint32_t n, ii;

size_t sensor_mode;

int threadStatus;
pthread_t timer_thread;
pthread_mutex_t get_position_mutex;
pthread_mutex_t set_position_mutex;

bool isLarge;
int teamID;

// a struct to hold the position of the ball
struct BallPosition {
	short x_pos;
	short y_pos;
};
struct BallPosition ballPosition;

// a struct to hold our current position
struct Position{
	short x_pos;
	short y_pos;
	int heading;
};

struct Position myP = {.x_pos = 0, .y_pos = 0, .heading = 0};

// the number of tacho pulses on each wheel
struct WheelPosition{
	int left_pos;
	int right_pos;
	int angle;
};

// intial values to signal that this hasn't yet been properly set 
struct WheelPosition myIP = {.left_pos = -9000, .right_pos = -9000, .angle = -9000}; 
struct WheelPosition prevPos = {.left_pos = 0, .right_pos = 0, .angle = 0};

unsigned char ally = 0;
unsigned char side = 0;
unsigned char hasStarted = 0;
int s;
uint16_t msgId = 0;
uint16_t servMsgId = 0;
uint16_t lastMsgID = 0;
unsigned char lastSender = 0;
char ballAction;
unsigned char role;
short firstX = -7000;
short firstY = -7000;

// Used to set the first position based on where we start in the arena
void set_initial_position () {
	// note the number of pulses on each wheel when we start
	int left_pos, right_pos;
	get_tacho_position(sn_wheels[L], &left_pos);
	get_tacho_position(sn_wheels[R], &right_pos);
	myIP.left_pos = left_pos;
	myIP.right_pos = right_pos;

	// get the first angle we're facing
	float angle = get_sensor_val0_float(sn_gyro);
	myIP.angle = angle;

	// set our previous position - actually the number of tacho pulses - to this inital count of pulses
	prevPos.left_pos = myIP.left_pos;
	prevPos.right_pos = myIP.right_pos;
	prevPos.angle = myIP.angle;

	// beginner initial position
	if (role == 0) {
		myP.x_pos = 18;
		myP.y_pos = 27;

	}
	// finisher initial position
	else {
			myP.x_pos = 102;
			myP.y_pos = 373;
	}
}

// sin with degrees instead of radians
double sind(double angle)
{
    double angleradians = angle * M_PI / 180.0f;
    return sin(angleradians);
}

// cos with degrees instead of radians
double cosd(double angle)
{
    double angleradians = angle * M_PI / 180.0f;
    return cos(angleradians);
}

// Set our position, based on:
// http://www.robotnav.com/position-estimation/
// and
// http://www.legoengineering.com/measuring-area-with-a-robot-part-one-follow-the-path/
void set_position () {
	int left_pos, right_pos, delta_left_pos, delta_right_pos;

	// if initial position isn't set, set it
	if (myIP.left_pos == -9000 || myIP.right_pos == -9000 || myIP.angle == -9000){
		set_initial_position();
	}

	get_tacho_position(sn_wheels[L], &left_pos);
	get_tacho_position(sn_wheels[R], &right_pos);
	delta_left_pos = left_pos - prevPos.left_pos;
	delta_right_pos = right_pos - prevPos.right_pos;
	prevPos.left_pos = left_pos;
	prevPos.right_pos = right_pos;


	double distance = ((delta_left_pos + delta_right_pos) * ENCODER_SCALE_FACTOR) / 2.0;
	int heading = get_sensor_val0_float(sn_gyro);
	prevPos.angle = myP.heading;

	short x_pos = myP.x_pos, y_pos = myP.y_pos;
	int delta_x = distance * sind(heading);
	int delta_y = distance * cosd(heading);

	// necessary to make x,y go in the right direction
	if (role == 1){
		delta_y *= -1;
		delta_x *= -1; 
	}
	
	x_pos += delta_x;
	y_pos += delta_y;

	// make these operations atomic so we get x,y and heading from the same time
	pthread_mutex_lock(&set_position_mutex);
	myP.x_pos = x_pos;
	myP.y_pos = y_pos;
	myP.heading = heading;
	pthread_mutex_unlock(&set_position_mutex);
}

// Send our position to the server, approximately every 2 seconds (maybe slightly longer than 2 seconds)
void *two_timer() {
	while (true) {
		set_position();
		sendMessage(MSG_POSITION, 255);
		Sleep(2000);
	}
}

// Grab a float from a sensor
float get_sensor_val0_float(uint8_t sn) {
	float value;

	while ( !get_sensor_value0(sn, &value )) {
		printf("COULDNT GET SENSOR VALUE!\n");
		fflush(stdout);
	}
	
	return value;
}

// Grab an int from the sensor, for the color sensor
int get_sensor_val_int(uint8_t sn) {
	int vals[2] = {-1,-1};
	int ctr = 0;

	// This needs to be correct, because it's used to search for the ball, so make sure 2 separate readings
	// spaced by half a second are the same. If not, try again.
	while (true) {
		for (ctr = 0; ctr < 2; ctr++) {
			while ( !get_sensor_value( 0, sn, &vals[ctr] ) || ( vals[ctr] < 0 ) || ( vals[ctr] >= COLOR_COUNT )) {
				printf("COULDNT GET SENSOR VALUE!\n");
				fflush(stdout);
			}
			Sleep(500);
		}

		if (vals[0] == vals[1]){
			return vals[0];
		}
	}
	return -1;
}

// Get what color the sensor is reading
const char * get_color() {

	int val = get_sensor_val_int(sn_color);

	if ( !val || ( val < 0 ) || ( val >= COLOR_COUNT )) {
		val = 0;
	}

	return color[ val ];
}

// determine if we're reading a specific color
bool is_color(char * colorToSee) { 
	const char * seen_color = get_color(sn_color);

	return !strcmp(seen_color, colorToSee);
}

// relase the ball from the claw
void release_ball() {

	lift_claw(SLOW_CLAW_SPEED_MULTIPLIER, SLOW_CLAW_SLEEP_TIME, CLAW_PULSES);
	go_straight_distance(BACKUP_DISTANCE);
	drop_claw(SLOW_CLAW_SPEED_MULTIPLIER, SLOW_CLAW_SLEEP_TIME, CLAW_PULSES);

	// reset motor
	Sleep(500);
	set_tacho_command_inx(sn_motor, TACHO_RESET);
	Sleep(700);
}

// run the claw for a given amount of tacho pulses
void run_claw_to_rel_pos(double speed, int pos, int claw_sleep_time) {
	float m_speed= speed * max_speed_motor;
    set_tacho_speed_sp( sn_motor, m_speed);
    set_tacho_position_sp( sn_motor, pos);
    set_tacho_stop_action_inx( sn_motor, TACHO_HOLD);
    set_tacho_command_inx( sn_motor, TACHO_RUN_TO_REL_POS );
    Sleep(claw_sleep_time);

    return;
}

// lift the claw up 
void lift_claw(float claw_speed_multiplier, int claw_sleep_time, int claw_pulses) {

	printf("in lift claw, pulses=%d\n", -1 * claw_pulses);
	fflush(stdout);
	run_claw_to_rel_pos(claw_speed_multiplier, -1 * claw_pulses, claw_sleep_time);

	return;
}

// drop the claw
void drop_claw(float claw_speed_multiplier, int claw_sleep_time, int claw_pulses) {

	printf("in drop claw, pulses=%d\n", claw_pulses);
	run_claw_to_rel_pos(claw_speed_multiplier, claw_pulses, claw_sleep_time);
	return;
}

// get distance sensor reading
float get_distance() {
	return get_sensor_val0_float(sn_sonar) / 10.0;
}

// turn in a direction at an angle
void turn(int direction, float angle) {

	// update our position
	Sleep(100);
	set_position();

     if (direction == R) {
     	angle *= -1;
     }
    
     run_to_rel_pos(SPEED_CIRCULAR_MULTIPLIER, DEGREE_TO_COUNT(-angle), SPEED_CIRCULAR_MULTIPLIER, DEGREE_TO_COUNT(angle));

     return;
}

// run wheels to a specific number of tacho pulses
void run_to_rel_pos(double l_speed, int l_pos, double r_speed, int r_pos) {
	float speed_l = l_speed * max_speed_wheelR;
	float speed_r = r_speed * max_speed_wheelR;

    set_tacho_speed_sp( sn_wheels[ L ], speed_l);
    set_tacho_speed_sp( sn_wheels[ R ], speed_r);

    set_tacho_position_sp( sn_wheels[ L ], l_pos );
    set_tacho_position_sp( sn_wheels[ R ], r_pos );
	multi_set_tacho_stop_action_inx( sn_wheels, TACHO_HOLD );
    multi_set_tacho_command_inx( sn_wheels, TACHO_RUN_TO_REL_POS );
    Sleep(200);

    // hang out in this loop while the wheels are running
 	do {
		get_tacho_state_flags( sn_wheels[ L ], &state_wheel_L );
		get_tacho_state_flags( sn_wheels[ R ], &state_wheel_R );
	} while ( state_wheel_L != TACHO_HOLDING && state_wheel_R != TACHO_HOLDING);

    return;
}

// go straight distance cms
void go_straight_distance(float distance) {

	// update our position
	Sleep(1500);
	set_position();

	int pulses = DISTANCE_TO_PULSES(distance); 
	run_to_rel_pos(SPEED_LINEAR_MULTIPLIER, pulses, SPEED_LINEAR_MULTIPLIER, pulses);

	return;
}


// grab the ball. returns the distance in cms, that it went forward to grab the ball
float grab_ball() {

	// either go a constant distance forward to grab, or slightly more than the distance sensor - 
	// whichever is smaller
	float distance = get_distance();
	if (distance > OFFSET_FROM_BALL) {
		distance = OFFSET_FROM_BALL;
	}
	else {
		distance = distance + 3;
	}

	// lift claw and grab ball
	lift_claw(FAST_CLAW_SPEED_MULTIPLIER, FAST_CLAW_SLEEP_TIME, CLAW_PULSES);
	go_straight_distance(distance);
	drop_claw(FAST_CLAW_SPEED_MULTIPLIER, FAST_CLAW_SLEEP_TIME, CLAW_PULSES);

	// reset the claw motor so it's not stuck too close to the ground
	Sleep(500);	
	set_tacho_command_inx(sn_motor, TACHO_RESET);
	Sleep(700);

	// return how far forward we went
	return distance;
}

// search for and grab the ball. returns how far it went while searching and grabbing.
float grab_ball_new() {
	float distanceToGrab = 0;
	turnAngle = 0;
	char * currColor;
	float distance;

	// only search 60cm forward, otherwise just give up
	while (distanceToGrab <= 60) {
		int ctr;
		// search from 0 - 60 degrees left
		for (ctr = 0; ctr < 6; ctr++) { 
			currColor = get_color();
			distance = get_distance();

			// if we see the ball color, grab the ball. sometimes the blue ball reads as green.
			if (( !strcmp(currColor, "RED") || !strcmp(currColor, "GREEN") || !strcmp(currColor, "BLUE"))) {
				distanceToGrab += grab_ball();
				return distanceToGrab;
			}

			// didn't see the ball, so turn 10 degrees
			else {
				turn(L, 10);
				turnAngle += 10;
			}
		}

		// turn back to 0
		turn(R, 60); 
		turnAngle = 0;

		//this is the same as above, just turning right
		for (ctr = 0; ctr < 6; ctr++) {
			currColor = get_color();
			distance = get_distance();
			if (( !strcmp(currColor, "RED") || !strcmp(currColor, "GREEN") || !strcmp(currColor, "BLUE"))) {
				distanceToGrab += grab_ball();
				turnAngle *= -1;
				return distanceToGrab;
			}

			else {
				turn(R, 10);
				turnAngle += 10;
			}
		}

		// turn back to 0
		turn(L, 60); 
		turnAngle = 0;

		// didn't find the ball in this scan, so move forward 3 cms and try again
		go_straight_distance(3);
		distanceToGrab += 3;	
	}

	// return how far forward the process took us
	return distanceToGrab; 
}


// poll the server for messages, returning how many bytes were read
int read_from_server (int sock, char *buffer, size_t maxSize) {
    int bytes_read = read (sock, buffer, maxSize);

    if (bytes_read <= 0) {
        printf("Server unexpectedly closed connection...\n");
        close (s);
        exit (EXIT_FAILURE);
    }

    return bytes_read;
}

// parse a message received from the server
void parseMessage (const unsigned char *buf, int nbbytes) {
    uint16_t id;

    if (buf[3] != teamID) {
        printf("*** This message is not for me (%d) ***\n", buf[3]);
        return;
    }

    id = *((uint16_t *) buf);
    lastMsgID = id;

    printf("[id: %4d, from: %3d]  ", (unsigned char) id, buf[2]);
    if (buf[2] != 255)
        lastSender = buf[2];

    switch (buf[4]) {
        case MSG_ACK:
            {
                /* ACK */
                uint16_t idAck;

                idAck = *((uint16_t *) &buf[5]);

                printf("ACK      idAck=%d status=%d\n", idAck, buf[7]);

                break;
            }
        case MSG_NEXT:
            {
                /* NEXT */
                msgToAck = id;
                printf("NEXT\n");

                // next on right large side means finisher must ACK, run its routine, then send NEXT
                sendMessage(MSG_ACK, ally);
            	finisher_large_right();
            	sendMessage(MSG_NEXT, teamID);

                break;
            }
        case MSG_START:
            {
                /* START */
            	// reset the gyro sensor
                set_sensor_mode(sn_gyro, "GYRO-ANG");
				Sleep(200);
				set_sensor_mode(sn_gyro, "GYRO-G&A");
				Sleep(200);

				role = buf[5];
                side = buf[6];
                ally = buf[7];

                printf("START    role=%s  side=%s   ally=%d\n", (role? "FINISHER" : "BEGINNER"), (side ? "LEFT " : "RIGHT"), ally);

            
                hasStarted = 1;

                // if we're the beginner, run that routine
                if (role == 0) {
                	beginner_large_right();
                }
                return;
            }
        case MSG_STOP:
            {
                /* STOP */
               printf("STOP\n");

                close(s);
                exit (0);

                return;
            }
        case MSG_CUSTOM:
            {
                /* CUSTOM */

                int i;

 				msgToAck = id;
                printf("CUSTOM   content=");
                for (i=5; i<nbbytes; i++) {
                    printf("%02X", buf[i]);
                    if ((i-5) % 4 == 3)
                        printf(" ");
                }
                printf("\n");

                break;
            }
        case MSG_KICK:
            {
                /* KICK */
                printf("KICK     id=%d\n", buf[5]);

                if (buf[5] == teamID) {
                    close(s);
                    exit (0);
                } else if (buf[5] == ally) {
                    //isLeader = 1;
                }

                return;
            }
        case MSG_BALL:
            {
                /* BALL */
                int16_t x, y;

                msgToAck = id;

                x = *((int16_t *) &buf[6]);
                y = *((int16_t *) &buf[8]);

                printf("BALL     %s x=%d y=%d\n", buf[5] ? "PICK" : "DROP", x, y);

                ballPosition.x_pos = x;
                ballPosition.y_pos = y;

                if (lastSender == teamID) {
                	sendMessage(MSG_ACK, teamID); 
                }

                else {
                	sendMessage(MSG_ACK, ally);
                }

                break;
            }
        default:
            {
                printf("*** unkown message type 0x%02X ***\n", buf[4]);
                return;
            }
    }
}


pthread_t tid;
int rankSelect;

#define MAXPARAM        5

struct field {
    unsigned int value;
    char sign;
} fields[MAXPARAM];

struct {
    char *name;
    struct {
        char *name;
        int size;
    } params [MAXPARAM];
} protocol [] = {
    {"ACK", {
                {"dst", 1},
                {"ID ack", 2},
                {"state", 1},
                {NULL, 0},
                {NULL, 0}
            }
    },
    {"NEXT", {
                 {"dst", 1},
                 {NULL, 0},
                 {NULL, 0},
                 {NULL, 0},
                 {NULL, 0}
             }
    },
    {"START", {
                  {"dst", 1},
                  {"role", 1},
                  {"side", 1},
                  {"ally", 1},
                  {NULL, 0}
              }
    },
    {"STOP", {
                 {"dst", 1},
                 {NULL, 0},
                 {NULL, 0},
                 {NULL, 0},
                 {NULL, 0}
             }
    },
    {NULL, {
                 {NULL, 0},
                 {NULL, 0},
                 {NULL, 0},
                 {NULL, 0},
                 {NULL, 0}
             }
    },
    {"KICK", {
                 {"dst", 1},
                 {"id", 1},
                 {NULL, 0},
                 {NULL, 0},
                 {NULL, 0}
             }
    },
    {"POSITION", {
                   {"dst", 1},
                   {"x", 2},
                   {"y", 2},
                   {NULL, 0},
                   {NULL, 0}
               }
    },
    {"BALL", {
                   {"dst", 1},
                   {"act", 1},
                   {"x", 2},
                   {"y", 2},
                   {NULL, 0}
               }
    }
};

// send a message over bluetooth
void sendMessage (char msgType, char dst) {
    char string[58];
    int index = 5;
    short xToSend;
    short yToSend;

    if (msgType == MSG_POSITION) {
        *((uint16_t *) string) = servMsgId++;
    } else {
        *((uint16_t *) string) = msgId++;
    }
    string[2] = teamID;
    string[3] = dst;
    string[4] = msgType;
    switch (msgType) {

    	case MSG_ACK:
    		printf("\n\nally is: %d\n\n", ally);
    		*((uint16_t *) (string+5)) = msgToAck;
    		string[7] = 0;
    	    fflush(stdout);

    		index = 8;
    		break;
    	case MSG_NEXT:
    		index = 5;
    		break;
    	case MSG_START:
    		index = 8;
    		break;
    	case MSG_STOP:
    		index = 5;
    		break;
    	case MSG_CUSTOM:
    		break;
    	case MSG_KICK:
    		index = 6;
    		break;
    	case MSG_POSITION:
    		// get the position without being interrupted, so the x,y correspond to the same time frame
			pthread_mutex_lock(&get_position_mutex);
			xToSend = myP.x_pos;
			yToSend = myP.y_pos;
    		pthread_mutex_unlock(&get_position_mutex);

    		*((uint16_t *) (string+5)) = xToSend;
    		*((uint16_t *) (string+7)) = yToSend;

    		printf("x,y: %d,%d\n", xToSend, yToSend);
    		fflush(stdout);
    		index = 9;
    		break;
    	case MSG_BALL:
			// get the position without being interrupted, so the x,y correspond to the same time frame
			pthread_mutex_lock(&get_position_mutex);
			xToSend = myP.x_pos;
			yToSend = myP.y_pos;
    		pthread_mutex_unlock(&get_position_mutex);

    		*((uint16_t *) (string+6)) = xToSend;
    		*((uint16_t *) (string+8)) = yToSend;

    		printf("\n\nBALL - x,y: %d,%d\n\n", xToSend, yToSend);
    		fflush(stdout);

    		string[5] = ballAction;
    		index = 10;
    		break;
    }

    fflush(stdout);
    write(s, string, index);
	
}

// Runs from finisher start to destination, grabbing the ball
void finisher_large_right() {
	// start a thread to send our position every 2 seconds
	threadStatus = pthread_create(&timer_thread, NULL, two_timer, NULL);
    if (threadStatus != 0) {
		printf("TIMER IS NOT WORKING!\n");
		exit(-1);
	}

	// navigate to the ball area
	go_straight_distance(80);
	turn(R, 90);
	go_straight_distance(80);
	turn(L,90);
	go_straight_distance(100);
	turn(L,90);

	// search for the ball
	float distanceGone = grab_ball_new();

	// turn back to the side wall and move towards it, to line up with finisher destination
	turn(R, turnAngle);
	go_straight_distance(73-distanceGone);

	// turn towards destination and go there
	turn(R,90);
	go_straight_distance(200-10-25);

	// let ally know we're done 
	sendMessage(MSG_NEXT, ally);

	// stop sending position messages
	pthread_cancel(timer_thread);

}

// run robot from beginner start to destination, dropping the ball
void beginner_large_right() {

	// start a thread that sends our position every 2 seconds 
	threadStatus = pthread_create(&timer_thread, NULL, two_timer, NULL);
    if (threadStatus != 0) {
		printf("TIMER IS NOT WORKING!\n");
		exit(-1);
	}

	// navigate to the ball area
	go_straight_distance(80);
	turn(R, 90);
	go_straight_distance(70);
	turn(L,90);
	go_straight_distance(100);

	// turn toward the ball area and advance to it
	turn(L, 90);
	go_straight_distance(40);

	// turn around so that we can back away from the ball without hitting it
	turn(L,180);

	// let our ally know we've dropped the ball
	sendMessage(MSG_BALL, ally);

	// wait until ball has stopped rolling and release it
	Sleep(3000);
	release_ball();

	// head towards destination
	turn(L, 90);
	Sleep(1000);
	go_straight_distance(165);
	Sleep(1000);

	// let our ally know we've reached the destination
	sendMessage(MSG_NEXT, ally);

	// stop sending our position to the server
	pthread_cancel(timer_thread);
	
}

void usage (const char * execname) {
    fprintf (stderr, "Usage: %s\n", execname);
    exit (EXIT_FAILURE);
}


int main( int argc,  char ** argv) {

#ifndef __ARM_ARCH_4T__
	/* Disable auto-detection of the brick (you have to set the correct address below) */
	ev3_brick_addr = "192.168.0.204";

#endif
	if ( ev3_init() == -1 ) return ( 1 );

#ifndef __ARM_ARCH_4T__
	printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );

#else
	printf( "Waiting tacho is plugged...\n" );

#endif


    teamID = TEAM_ID;

	/* Set up tacho motors and store in sn variables */
	while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

	printf( "*** ( EV3 ) Hello! ***\n" );

	printf( "Found tacho motors:\n" );
	printf("DESC_LIMIT: %d", DESC_LIMIT);

	for (int i = 0; i < DESC_LIMIT; i++ ) {
		if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
			printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
			printf( "  port = %s\n", ev3_tacho_port_name( i, strBuf ));
			printf( "  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i)); 
		}
	}


	int port=A;
	for (port=A; port<69; port++){
		if (port == B) {
			printf("\nhi B1\n");
			if ( ev3_search_tacho_plugged_in(port,0, &sn_wheels[R], 0 )) {
				get_tacho_max_speed( sn_wheels[1], &max_speed_wheelR );
				printf("\nhi B2\n");

			}
		}
		if (port == C) {
			printf("\nhi C1\n");
			if ( ev3_search_tacho_plugged_in(port,0, &sn_motor, 0 )) {
				get_tacho_max_speed( sn_motor, &max_speed_motor );
				printf("\nhi C2\n");
			}
		}
		if (port == D) {
			printf("\nhi D1\n");
			if ( ev3_search_tacho_plugged_in(port,0, &sn_wheels[L], 0 )) {
				get_tacho_max_speed( sn_wheels[1], &max_speed_wheelL );
				printf("\nhi D2\n");

			}
		}
	}

	// reset the motors so they are at a neutral position
	multi_set_tacho_command_inx(sn_wheels, TACHO_RESET);
	set_tacho_command_inx(sn_motor, TACHO_RESET);

	
	/* Set up sensors and store in sn variables */
	ev3_sensor_init();

	printf( "Found sensors:\n" );
	for (i = 0; i < DESC_LIMIT; i++ ) {
		if ( ev3_sensor[ i ].type_inx != SENSOR_TYPE__NONE_ ) {
			printf( "  type = %s\n", ev3_sensor_type( ev3_sensor[ i ].type_inx ));
			printf( "  port = %s\n", ev3_sensor_port_name( i, strBuf ));
			if ( get_sensor_mode( i, strBuf, sizeof( strBuf ))) {
				printf( "  mode = %s\n", strBuf );
			}
			if ( get_sensor_num_values( i, &n )) {
				for (ii = 0; ii < n; ii++ ) {
					if ( get_sensor_value( ii, i, &val )) {
						printf( "  value%d = %d\n", ii, val );
					}
				}
			}
		}
	}

	ev3_search_sensor( LEGO_EV3_TOUCH, &sn_touch, 0 );
 	ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0 );
	ev3_search_sensor(HT_NXT_COMPASS, &sn_compass,0);
	ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0);
	ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro,0);

	// set color sensor to color mode
	set_sensor_mode(sn_color, "COL-COLOR");
	Sleep(200);

	// reset the gyroscope
	set_sensor_mode(sn_gyro, "GYRO-ANG");
	Sleep(200);
	set_sensor_mode(sn_gyro, "GYRO-G&A");
	Sleep(200);

	/******/
	// Bluetooth code below

	Sleep(500);

	struct sockaddr_rc addr = { 0 };
    int status;

    /* allocate a socket */
    s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

    /* set the connection parameters (who to connect to) */
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_channel = (uint8_t) 1;
    str2ba (SERV_ADDR, &addr.rc_bdaddr);

    /* connect to server */
    status = connect(s, (struct sockaddr *)&addr, sizeof(addr));

    printf("about to connect to server\n");

    /* if connected */
    if( status == 0 ) {
        char string[58];
        int nbbytes;

        printf("connected to server\n");
        for (;;) {
            printf("reading froms server\n");
            nbbytes = read_from_server (s, string, 20);
            parseMessage ((unsigned char *) string, nbbytes);     
        }

    } else {
        printf("didn't connect to server\n");
        fprintf (stderr, "Failed to connect to server...\n");
        exit (EXIT_FAILURE);
    }

    close(s);

	ev3_uninit();

	printf( "*** ( EV3 ) Bye! ***\n" );

	// reset motors
	multi_set_tacho_command_inx(sn_wheels, TACHO_RESET);
	set_tacho_command_inx(sn_motor, TACHO_RESET);

	Sleep(3000);

	return 0;
}
