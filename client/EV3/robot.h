
/*
	Read a sensor that returns a float.
*/
float get_sensor_val0_float(uint8_t sn);

/*
	Read a sensor that returns an int.
*/
int get_sensor_val_int(uint8_t sn);

/*
	Return a pointer to a string holding a color name: "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" 
*/
const char * get_color();


/*
	Determine if colorToSee is currently being read by the sensor, and return true if so.
*/
bool is_color(char * colorToSee);

/*
	Drop the claw.
*/
void drop_claw(float claw_speed_multiplier, int claw_sleep_time, int claw_pulses); 

/*
	Lift the claw.
*/

void lift_claw(float claw_speed_multiplier, int claw_sleep_time, int claw_pulses);

/*
	Gets the distance from the sonar sensor in centimeters.
*/
float get_distance();

/*
	Determine if the ball is seen
*/
bool is_ball();

/*
	Run the wheels for the amount of pulses specified.
*/
void run_to_rel_pos(double l_speed, int l_pos, double r_speed, int r_pos);

/*
	Turn angle degrees
*/
void turn(int direction, float angle);


/*
	Go straight for distance centimeters.
*/
void go_straight_distance(float distance);

/* 	
	Grab the ball by lifting claw and moving forward, then dropping claw
*/
float grab_ball();

/*
	Release the ball by lifting claw, going backwards, and dropping the claw
*/
void release_ball();

/*
	Run the claw for a given number of tacho pulses (pos)
*/
void run_claw_to_rel_pos(double speed, int pos, int claw_sleep_time);


/*
	Set our initial position in the arena
*/
void set_initial_position ();

/*
	Set our current position in the arena
*/
void set_position ();

/* 
	sin that takes degrees
*/
double sind(double angle);

/* 
	cose that takes degrees
*/
double cosd(double angle);

/* 
	Send position to server every 2 seconds
*/
void *two_timer();

/* 
	Read a message from the server
*/
int read_from_server (int sock, char *buffer, size_t maxSize);

/* 
	Send a message to the server
*/
void sendMessage (char msgType, char dst);

/*
	Drop the ball in the large arena on the left side
*/
void beginner_large_left();

/*
	Drop the ball in the large arena on the right side
*/
void beginner_large_right();

/*
	Drop the ball in the small arena on the right side
*/
void beginner_small_right();
/*
	Drop the ball in the small arena on the left side
*/
void beginner_small_left();

/*
	Pick up the ball on the right side of the small arena
*/
void finisher_small_right();

/*
	Pick up the ball on the left side of the small arena
*/
void finisher_small_left();

/*
	Pick up the ball on the left side of the large arena
*/
void finisher_large_left();

/*
	Pick up the ball on the right side of the large arena
*/
void finisher_large_right();

/*
	Search for the ball using the color sensor and grab it
*/
float grab_ball_new();

