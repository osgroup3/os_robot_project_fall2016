# Eurebot
## Group 3: Di HAO, Nick MANFREDI, Luis Fernando MENDOZA
## EURECOM OS Robot Contest
## Main website: http://soc.eurecom.fr/OS/projects_fall2016.html

### FYI
####To build
The relevant source files are found in client/EV3 and are the following:

robotRightLarge.c

robotLeftLarge.c

robotRightSmall.c

robotLeftSmall.c


These can be compiled on the Docker image provided by the professor using the following makefiles, respectively (also in client/EV3):

DockerRightLarge

DockerLeftLarge

DockerRightSmall

DockerLeftSmall

Just type:

make -f [MakefileName]


Then, transfer the newly compiled executable to the robot using scp. 


####To run
For the beginner on the right side of the small arena, it is assumed the robot starts facing the "Destination Fin." square. Otherwise, the robot should be started facing forward (out of his square). 


####Workflow
Our workflow was as follows: all code was compiled either on the robot or, later, using Docker on Hao Di's computer. Typically, Both Hao Di and Nick Manfredi used Nick's machine to write and push code, which was then pulled onto the device which would compile it. This explains the strange commit history - most commits say "Nick", but code was also written by Hao Di (using Nick's computer). Also, every change to the code - whether it compiled or not - was pushed, in order to pull it onto the device that would compile it. Accordingly, there are a lot of commits for very small things, and the commit messages are not very descriptive.

